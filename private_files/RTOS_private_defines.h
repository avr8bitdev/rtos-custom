/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


// These are private/internal defines.


#ifndef RTOS_PRIVATE_DEFINES_H_
#define RTOS_PRIVATE_DEFINES_H_


#include "RTOS_private_types.h"
#include "../RTOS_config.h"

// 32 GP registers + SREG
#define RTOS_GP_REG_LEN (32 + 1)
// +2*sizeof(RTOS_PC_t) for return address of the task, and address of 'RTOS_vidResetCurrentTask'
#define RTOS_STACK_LEN (RTOS_USER_STACK_SIZE + RTOS_GP_REG_LEN + 2*sizeof(RTOS_PC_t))
// +1 for background task
#define RTOS_TASKS_LEN (RTOS_USER_TASKS_LEN + 1)
// size of the internal Playfield stack (in bytes)
#if (RTOS_USE_ERROR_CALLBACK == 0)
#define RTOS_PLAYFIELD_LENGTH 32
#else // give more space for callbacks
#define RTOS_PLAYFIELD_LENGTH 100
#endif // RTOS_USE_ERROR_CALLBACK


#endif /* RTOS_PRIVATE_DEFINES_H_ */

