Licensed under MIT License.

# RTOS (custom)

Custom-made RTOS with Fixed-Priority Preemptive Round-Robin Scheduling algorithm

# Some useful links:

- https://dmitryfrank.com/articles/how_i_ended_up_writing_my_own_kernel

- http://www.ddiinnxx.com/intro-real-time-operating-systems-rtos/

