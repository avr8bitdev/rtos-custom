/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#include "../../basic_includes/custom_types.h"
#include "RTOS_functions.h"
#include "RTOS_config.h"
#include "RTOS_public_types.h"
#include "private_files/RTOS_private_defines.h"
#include "private_files/RTOS_private_types.h"
#include "private_files/RTOS_private_objects.h"
#include "impl_specific_files/RTOS_compiler_specific.h"
#include "impl_specific_files/RTOS_mcu_specific.h"
#include "../../MCAL_drivers/Timer_driver/Timer_functions.h"
#include <avr/interrupt.h>

#ifdef RTOS_COOKIE_


// sanity checks
#if (RTOS_COOKIE_ < 0) || (RTOS_COOKIE_ > 2)
    #error "RTOS ERROR: RTOS_COOKIE_ is out of range, accepted values are: 0, 1, or 2."
#endif

#if !defined(RTOS_USE_EXTERNAL_STACK)
    #error "RTOS ERROR: RTOS_USE_EXTERNAL_STACK is undefined."
#endif // RTOS_USE_EXTERNAL_STACK

#if !defined(RTOS_USER_STACK_SIZE)
    #error "RTOS ERROR: RTOS_USER_STACK_SIZE is undefined."
#endif // RTOS_USER_STACK_SIZE

#if !defined(RTOS_USER_TASKS_LEN)
    #error "RTOS ERROR: RTOS_USER_TASKS_LEN is undefined."
#endif // RTOS_USER_TASKS_LEN

#if !defined(RTOS_OVERFLOW_CHECK)
    #error "RTOS ERROR: RTOS_OVERFLOW_CHECK is undefined."
#endif // RTOS_OVERFLOW_CHECK

#if !defined(RTOS_USE_ERROR_CALLBACK)
    #error "RTOS ERROR: RTOS_USE_ERROR_CALLBACK is undefined."
#endif // RTOS_USE_ERROR_CALLBACK


/*
 BUG: lock background task (on semaphore), and lock other tasks (or delay them)
      RTOS will hang at the switching loop, particularly in 'while (current_task->state != RTOS_state_idle) {...}'
*/
// TODO: fix this! (maybe don't allow locking of background task)


// --- internal --- //
// *** kernel functions prototypes *** /
RTOS_KERNEL_INLINE_DECL(RTOS_vidIncCurrentTaskPtr, void);

#if (RTOS_OVERFLOW_CHECK != 0)
RTOS_KERNEL_NOINLINE_DECL(RTOS_vidHandleOverflow, void);
#endif // RTOS_OVERFLOW_CHECK

#if (RTOS_USE_ERROR_CALLBACK != 0)
RTOS_KERNEL_NOINLINE_DECL(RTOS_vidHandleErrorCB, RTOS_state_t enumErrorCpy);
#endif // RTOS_USE_ERROR_CALLBACK

RTOS_KERNEL_NOINLINE_DECL(RTOS_vidGetNextTask, void);
RTOS_KERNEL_NOINLINE_DECL(RTOS_vidHandleDelete, register RTOS_task_t* taskPtrTaskCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_vidImplHandleDelete, register RTOS_task_t* taskPtrTaskCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_vidHandleDelay, register u16 u16Skip_msCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_vidHandleAcqBinSem, register RTOS_sem_handle_t semHandleCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_vidHandleRelBinSem, register RTOS_sem_handle_t semHandleCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_vidHandleAcqCountingSem, register RTOS_sem_handle_t semHandleCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_vidHandleEnterCriticalSection, void);
RTOS_KERNEL_NOINLINE_DECL(RTOS_vidHandleLeaveCriticalSection, void);
RTOS_KERNEL_NOINLINE_DECL(RTOS_vidHandlePause, register RTOS_task_t* taskPtrTaskCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_vidHandleReset, register RTOS_task_t* taskPtrTaskCpy);
RTOS_KERNEL_NOINLINE_DECL(RTOS_vidImplHandleReset, register RTOS_task_t* taskPtrTaskCpy);
// ************ //

// *** INLINE functions: these are spreaded in place where they're called *** //
// circular increment of 'current_taks' pointer
RTOS_KERNEL_INLINE(RTOS_vidIncCurrentTaskPtr, void)
{
    // point at next task
    current_task += 1;
    // circular increment
    if (current_task == (tasks + RTOS_TASKS_LEN))
    {
        current_task = &(tasks[0]);
    }
}
// ************ //

// *** NOINLINE functions: these are handler functions, usually called  from a 'naked' function *** //
// overflow check
#if (RTOS_OVERFLOW_CHECK != 0)
RTOS_KERNEL_NOINLINE(RTOS_vidHandleOverflow, void)
{
    if (current_task == background_task)
    {
        return;
    }
    
    // if (stack overflow happened)
    // if (current stack addr < max allowed top addr) -> stack grows from bot to top
    if ( (current_task->stack_ptr) < (current_task->stack_top) )
    {
        current_task->state = RTOS_state_ERROR_Stack_Overflow; // set state as 'Stack Overflow'

#if (RTOS_USE_ERROR_CALLBACK != 0)
        RTOS_vidHandleErrorCB(RTOS_state_ERROR_Stack_Overflow);
#endif // RTOS_USE_ERROR_CALLBACK
    }
}
#endif // RTOS_OVERFLOW_CHECK

// error-handling callback system
#if (RTOS_USE_ERROR_CALLBACK != 0)
RTOS_KERNEL_NOINLINE(RTOS_vidHandleErrorCB, RTOS_state_t enumErrorCpy)
{
    register RTOS_action_t error_action = RTOS_action_NO_CB;

    if (isInsideCriticalSection)
    {
        isInsideCriticalSection = 0;
        
        if (RTOS_CB_critical_error)
        {
            error_action = RTOS_CB_critical_error((u8)(current_task - tasks), enumErrorCpy);
        }
    }
    else
    {
        if (RTOS_CB_error)
        {
            error_action = RTOS_CB_error((u8)(current_task - tasks), enumErrorCpy);
        }
    }
    
    if (error_action != RTOS_action_NO_CB)
    {
        switch (error_action)
        {
            case RTOS_action_delete:
                RTOS_vidImplHandleDelete(current_task);
            break;

            case RTOS_action_reset:
                RTOS_vidImplHandleReset(current_task);
            break;

            case RTOS_action_continue:
            default:
            break;
        }
    }
}
#endif // RTOS_USE_ERROR_CALLBACK

// switching algorithm
RTOS_KERNEL_NOINLINE(RTOS_vidGetNextTask, void)
{
    // --- update current task state
    // only 'resumed' tasks are set to 'idle',
    // needed because 'delayed', 'blocked', etc... tasks must not be set to 'idle' blindly
    // also other states indicate either error-ed or paused state
    if (current_task->state == RTOS_state_resumed)
    {
        current_task->state = RTOS_state_idle;
    }
    // ------ //

    // --- update all tasks states and detect higher priority or abandoned ones
    // those are place-holders to indicate the result of the next loop
    static RTOS_task_t* abandoned_task = 0; // points at a previously abandoned task (if any)
    static u8 isAbandonedFound = 0;

    // if (we're not inside a critical section), then point at abandoned task (if any) or get next available task
    if (!isInsideCriticalSection)
    {
        // if (we previously abandoned a task), then point at it
        // all calculations (previous, etc...) should be relative to it's position
        if (isAbandonedFound)
        {
            current_task = abandoned_task;
            isAbandonedFound = 0;
        }
        else // if (no abandoned task found), then get the next available task
        {
            // get the next available task ('idle' or 'resumed')
            do
            {
                RTOS_vidIncCurrentTaskPtr();
            }
            while ( (current_task->state != RTOS_state_idle) && (current_task->state != RTOS_state_resumed) );

            // declare current task as 'resumed' (in case it was 'idle')
            current_task->state = RTOS_state_resumed;
        }
    }

    RTOS_task_t* previous_task = 0;  // points at previous task that wants to run (if any)
    u8 isPreviousFound = 0; // indicates whether 'previous_task' was already set or not
                            // needed because we want to catch the very-first previous task,
                            // hence, it prevents multiple assignments

    // cache the value of the volatile ticks counter since it won't change during the below loop
    register u16 RTOS_u16Ticks_ms_cached = (u16)RTOS_u16Ticks_ms;

    // adjust states of all tasks,
    // and detect the first previous task that wants to run (if any)
    for (register RTOS_task_t* this_task = &(tasks[0]); this_task < (tasks + RTOS_TASKS_LEN); this_task += 1)
    {
        // take an action based on each state
        switch (this_task->state)
        {
            case RTOS_state_delayed: // if this task is delayed (triggered by a timeout)
                // this is a circular counter, i.e. 0 == (0xFFFF + 1) -> (MAX + 1)
                // ex: if current ticks = 0xFFFF, and required delay = 3 ticks
                //     therefore task->ticks = 0xFFFF + 3 = 2
                //     and when subtracting:
                //     1) 0 - 2 = (0xFFFF + 1) - 2 = 0xFFFF - 1 = 0xFFFE (not zero)
                //     2) 1 - 2 = (0xFFFF + 2) - 2 = 0xFFFF + 0 = 0xFFFF (not zero)
                //     3) 2 - 2 = (0xFFFF + 3) - 2 = 0xFFFF + 1 = 0      (zero)
                // but max delays is 65535

                // the XOR value will be 0 when: 'RTOS_u16Ticks_ms' = 'this_task->ticks'
                if ( !(RTOS_u16Ticks_ms_cached ^ this_task->ticks) ) // ticks are finished
                {
                    this_task->state = RTOS_state_resumed;
                }
            break;

            case RTOS_state_suspended: // if this was a delayed function, but currently 'suspended'
                if (this_task->last_state == RTOS_state_delayed)
                {
                    // the XOR value will be 0 when: 'RTOS_u16Ticks_ms' = 'this_task->ticks'
                    if ( !(RTOS_u16Ticks_ms_cached ^ this_task->ticks) ) // ticks are finished
                    {
                        this_task->last_state = RTOS_state_resumed;
                    }
                }
            break;

            case RTOS_state_blocked_bin_count_sem: // if this task is blocked (waiting for a semaphore)
                if ( *(this_task->sem_addr) ) // if semaphore has counts (isn't completely acquired anymore)
                {
                    this_task->state = RTOS_state_resumed;

                    // acquire a count from this semaphore, because this task will use it
                    *(this_task->sem_addr) -= 1;
                }
            break;

            default: // ignore all other states (suppresses compiler warning)
            break;
        }

        // from this point, the task state is updated, next if it became 'resumed'
        // then detect if it has higher priority

        // detect first higher priority task that wants to run (if any)
        if ( (this_task->state == RTOS_state_resumed) )
        {
            if (!isPreviousFound)
            {            
                if (this_task < current_task)
                {
                    previous_task = this_task;
                    isPreviousFound = 1;
                }
            }
        }
    }
    // ------ //

    // --- point at the task whose turn is now
    // if we're inside critical section, then point at the critical task
    // else if there's a higher priority task wants to run, then point at it,
    // else, do nothing, which means run the currently selected task
    if (isInsideCriticalSection)
    {
        current_task = critical_task;

        // always set the task state as 'critical running', because during the critical section the task
        // might do illegal actions that result in a state != 'critical running' (delayed, blocked sem, etc...),
        // hence, the state must be re-set back to 'critical running' whenever a context switch happens.
        // this ensures correct task state, but obviously if an illegal action happened during a critical section,
        // it will be missed.

        // TODO: from the previous description, check first to see if the state stayed 'critical running',
        //       if not, then maybe call a handler/callback
        current_task->state = RTOS_state_critical_running;
    }
    else if (isPreviousFound)
    {
        // declare the current/available task as abandoned, for later round
        abandoned_task = current_task;
        isAbandonedFound = 1;

        // point at the higher priority task
        current_task = previous_task;
    }
    // ------ //
}

// switch context if the deleted task was the current one
RTOS_KERNEL_NOINLINE(RTOS_vidHandleDelete, register RTOS_task_t* taskPtrTaskCpy)
{
    // deleting background task is NOT allowed
    if (taskPtrTaskCpy == background_task)
    {
        return;
    }

    // sanity check
    if (taskPtrTaskCpy >= (tasks + RTOS_USER_TASKS_LEN))
    {
        return;
    }

    RTOS_vidImplHandleDelete(taskPtrTaskCpy);

    // if this was a critical task, then leave the critical section
    if (isInsideCriticalSection && (taskPtrTaskCpy == critical_task))
    {
        isInsideCriticalSection = 0;
    }

    // if the deleted task was the current one, then do a context switch
    if (taskPtrTaskCpy == current_task)
    {
        RTOS_vidGetNextTask();
    }
}

// implementation for 'RTOS_vidHandleDelete(...)', also used by error callbacks when deleting
RTOS_KERNEL_NOINLINE(RTOS_vidImplHandleDelete, register RTOS_task_t* taskPtrTaskCpy)
{
    // set task state to 'uninitialized'
    taskPtrTaskCpy->state = RTOS_state_uninitialized;
}

// switch context if a task wants to delay
RTOS_KERNEL_NOINLINE(RTOS_vidHandleDelay, register u16 u16Skip_msCpy)
{
    // delaying the background task is NOT allowed
    if (current_task == background_task)
    {
        return;
    }

    if (u16Skip_msCpy)
    {
        // +1 because this a "length" model rather than a "distance" model.
        // ex: if we're at time slice 't', and we want to skip by 1,
        //     in "length" model: skip by 1 means skip 't+1' and start at 't+2'
        //     in "distance" model: skip by 1 means skipping the entire distance between
        //                          't' and 't+1', and start at 't+1'
        // this is why +1 is added to the below expression, to make it a "length" model.
        // notice how the "length" model is ahead of the "distance" model by '1'

        current_task->ticks = RTOS_u16Ticks_ms + u16Skip_msCpy + 1;
        current_task->state = RTOS_state_delayed;

        // get the next ready task
        RTOS_vidGetNextTask();
    }
}

// block task and switch context if binary semaphore is already acquired
RTOS_KERNEL_NOINLINE(RTOS_vidHandleAcqBinSem, register RTOS_sem_handle_t semHandleCpy)
{
    if ( !(*semHandleCpy) ) // if semaphore is already acquired (no counts left), then block task and switch context
    {
        current_task->state = RTOS_state_blocked_bin_count_sem;

        // save semaphore's address for later, so we can check later if semaphore is released
        current_task->sem_addr = semHandleCpy;

        // get next the available task
        RTOS_vidGetNextTask();
    }
    else // if semaphore is free (still has counts), then set it as acquired
    {
        *semHandleCpy = 0;
    }
}

// put task in error state and switch context if binary semaphore is already free
RTOS_KERNEL_NOINLINE(RTOS_vidHandleRelBinSem, register RTOS_sem_handle_t semHandleCpy)
{
    if ( !(*semHandleCpy) ) // if semaphore is acquired, then set it as free
    {
        *semHandleCpy = 1;
    }
    else // if semaphore is already free, then put task in error state and do a context switch
    {
        current_task->state = RTOS_state_ERROR_Bin_Sem_Already_Free;

#if (RTOS_USE_ERROR_CALLBACK != 0)
        RTOS_vidHandleErrorCB(RTOS_state_ERROR_Bin_Sem_Already_Free);
#endif // RTOS_USE_ERROR_CALLBACK

        // get next the available task
        RTOS_vidGetNextTask();
    }
}

// block task and switch context if semaphore is completely acquired
RTOS_KERNEL_NOINLINE(RTOS_vidHandleAcqCountingSem, register RTOS_sem_handle_t semHandleCpy)
{
    if ( !(*semHandleCpy) ) // if semaphore is completely acquired (no counts left), then block task and switch context
    {
        current_task->state = RTOS_state_blocked_bin_count_sem;

        // save semaphore's address for later, so we can check later if semaphore is released
        current_task->sem_addr = semHandleCpy;

        // get next the available task
        RTOS_vidGetNextTask();
    }
    else // if semaphore still has counts (still free), then decrement its count (acquire a count)
    {
        *semHandleCpy -= 1;
    }
}

// enter a new critical section, switch context if entering multiple critical sections
RTOS_KERNEL_NOINLINE(RTOS_vidHandleEnterCriticalSection, void)
{
    if (!isInsideCriticalSection) // if (NOT inside a critical section), then enter critical section
    {
        isInsideCriticalSection = 1;
        critical_task = current_task;
        current_task->state = RTOS_state_critical_running;

    }
    else                          // if (already inside a critical section), then put task in error state and switch context
    {
        current_task->state = RTOS_state_ERROR_multi_critical_sections;

#if (RTOS_USE_ERROR_CALLBACK != 0)
        RTOS_vidHandleErrorCB(RTOS_state_ERROR_multi_critical_sections);
#endif // RTOS_USE_ERROR_CALLBACK

        // get next the available task
        RTOS_vidGetNextTask();
    }
}

// leave a previously entered critical section, switch context if leaving unentered critical section
RTOS_KERNEL_NOINLINE(RTOS_vidHandleLeaveCriticalSection, void)
{
    if (isInsideCriticalSection) // if (already inside a critical section), then leave it
    {
        isInsideCriticalSection = 0;
        current_task->state = RTOS_state_resumed;
    }
    else                         // if (NOT inside a critical section), then put task in error state and switch context
    {
        current_task->state = RTOS_state_ERROR_unentered_critical_section;

#if (RTOS_USE_ERROR_CALLBACK != 0)
        RTOS_vidHandleErrorCB(RTOS_state_ERROR_unentered_critical_section);
#endif // RTOS_USE_ERROR_CALLBACK

        // get next the available task
        RTOS_vidGetNextTask();
    }
}

// pause a given task, if it's current one, then switch context
// also if current task made a stack overflow, then switch context
RTOS_KERNEL_NOINLINE(RTOS_vidHandlePause, register RTOS_task_t* taskPtrTaskCpy)
{
    // pausing background task is NOT allowed
    if (taskPtrTaskCpy == background_task)
    {
        return;
    }

    // sanity check
    if (taskPtrTaskCpy >= (tasks + RTOS_USER_TASKS_LEN))
    {
        return;
    }

    // BUGFIX: pausing an already paused task will lock it forever!
    //         because the second pause will make 'last_state' = 'suspended'
    // ex: at start     -> last_state = idle, state = idle
    //     first cycle  -> state = resumed
    //     first pause  -> last_state = resumed, state = suspended
    //     second pause -> last_state = suspended, state = suspended (wrong)
    if ( (taskPtrTaskCpy->state == RTOS_state_uninitialized)
         ||(taskPtrTaskCpy->state == RTOS_state_suspended)
         || (taskPtrTaskCpy->state >= RTOS_state_ERROR) )
    {
        return;
    }

    // save current state and change it to 'suspended'
    // TODO: is this true ? this is a "blind" change of state, maybe more logic should be added
    taskPtrTaskCpy->last_state = taskPtrTaskCpy->state;
    taskPtrTaskCpy->state = RTOS_state_suspended;

    // if the paused task was the current one, then do a context switch
    if (taskPtrTaskCpy == current_task)
    {
        RTOS_vidGetNextTask();
    }
}

// switch context after resetting task attributes
RTOS_KERNEL_NOINLINE(RTOS_vidHandleReset, register RTOS_task_t* taskPtrTaskCpy)
{
    // returning from the background task is NOT allowed (because it must NOT be reset)
    if (taskPtrTaskCpy == background_task)
    {
        return;
    }

    // sanity check
    if (taskPtrTaskCpy >= (tasks + RTOS_USER_TASKS_LEN))
    {
        return;
    }

    RTOS_vidImplHandleReset(taskPtrTaskCpy);

    // if this was a critical task, then leave the critical section
    if (isInsideCriticalSection && (taskPtrTaskCpy == critical_task))
    {
        isInsideCriticalSection = 0;
    }

    // if this was the current task, then get the next available task
    if (taskPtrTaskCpy == current_task)
    {
        RTOS_vidGetNextTask();
    }
}

// implementation for 'RTOS_vidHandleReset(...)', also used by error callbacks when resetting
RTOS_KERNEL_NOINLINE(RTOS_vidImplHandleReset, register RTOS_task_t* taskPtrTaskCpy)
{
    // set current and previous states to 'idle' (ready to run)
    taskPtrTaskCpy->state = RTOS_state_idle;
    taskPtrTaskCpy->last_state = RTOS_state_idle;

#if (RTOS_USE_EXTERNAL_STACK == 0)
    const register RTOS_SP_t stk = stack[(u8)(taskPtrTaskCpy - tasks)]; // point at required stack
    const register u16 stk_len = RTOS_STACK_LEN;
#else
    const register RTOS_SP_t stk = taskPtrTaskCpy->stack_top; // point at required stack
    const register u16 stk_len = taskPtrTaskCpy->stack_user_length + RTOS_GP_REG_LEN + 2*sizeof(RTOS_PC_t);
#endif // RTOS_USE_EXTERNAL_STACK

    // clear stack (implicitly GP registers too)
    for (register RTOS_SP_t s = stk; s < (stk + stk_len - 2*sizeof(RTOS_PC_t)); s += 1)
    {
        *s = 0;
    }

    // store the return address (address of first instruction)
    *(RTOS_PC_t*)(stk + stk_len - 2*sizeof(RTOS_PC_t)) = taskPtrTaskCpy->function_top;

    // point before PC and GP regs, so when it's pop-ed, stack pointer points at the beginning
    // of the stack (pop-ing increases pointer)
    taskPtrTaskCpy->stack_ptr = (RTOS_SP_t)(stk + stk_len - 2*sizeof(RTOS_PC_t) - RTOS_GP_REG_LEN - 1);
}
// ************ //


// *** RTOS ISR *** //
// called when a compare-match occurs
#if (RTOS_COOKIE_ == 0)   // Timer0
ISR(TIMER0_COMP_vect, ISR_NAKED)
#elif (RTOS_COOKIE_ == 2) // Timer2
ISR(TIMER2_COMP_vect, ISR_NAKED)
#elif (RTOS_COOKIE_ == 1) // Timer1
ISR(TIMER1_COMPA_vect, ISR_NAKED)
#endif
{
    RTOS_SAVE_CURRENT_CONTEXT();

#if (RTOS_OVERFLOW_CHECK != 0)
    RTOS_vidHandleOverflow();
#endif // RTOS_OVERFLOW_CHECK

    RTOS_INC_TICKS_COUNTER();

    RTOS_vidGetNextTask();

    RTOS_RESTORE_NEW_CONTEXT();

    RTOS_RETI;
}
// ************ //
// ---------------- //


RTOS_API(void, RTOS_vidInit, void)
{
    // disable global INT flag
    RTOS_DISABLE_GINT();

    // setup timer (initially stopped)
#if (RTOS_COOKIE_ == 0)   // Timer0
    Timer_vidInit(Timer0_prescaler_off, Timer_mode_CTC, 0); // CTC mode
    Timer_vidSetCmpValue((u8)( (F_CPU / 64.0) * 1e-3 ) - 1, 0); // 1ms
#elif (RTOS_COOKIE_ == 2) // Timer2
    Timer_vidInit(Timer2_prescaler_off, Timer_mode_CTC, 2); // CTC mode
    Timer_vidSetCmpValue((u8)( (F_CPU / 64.0) * 1e-3 ) - 1, 2); // 1ms
#elif (RTOS_COOKIE_ == 1) // Timer1
    Timer1_vidInit(Timer1_prescaler_off, Timer1_mode_CTC_TOP_OCR1A); // CTC mode
    Timer1_vidSetCmpValue((u16)( (F_CPU / 8.0) * 1e-3 ) - 1, Timer1_cmp_OCR1A); // 1ms
#endif

    // reset all tasks
    for (register RTOS_task_t* t = &(tasks[0]); t < (tasks + RTOS_USER_TASKS_LEN); t += 1)
    {
        t->state = RTOS_state_uninitialized;
        t->ticks = 0;
        t->sem_addr = 0;
        t->stack_ptr = 0;
        t->stack_top = 0;
        t->stack_user_length = 0;
        t->function_top = 0;
        t->last_state = RTOS_state_uninitialized;
    }

    // clear internal stacks (implicitly GP registers too)
#if (RTOS_USE_EXTERNAL_STACK == 0)
    for (register u8 i = 0; i < RTOS_USER_TASKS_LEN; i++)
    {
        for (register RTOS_SP_t s = stack[i]; s < (stack[i] + RTOS_STACK_LEN); s += 1)
        {
            *s = 0;
        }
    }
#endif // RTOS_USE_EXTERNAL_STACK

    // init background task
    background_task->state = RTOS_state_idle;
    background_task->ticks = 0;
    background_task->sem_addr = 0;
    background_task->stack_ptr = 0;
    background_task->stack_top = 0;
    background_task->stack_user_length = 0;
    background_task->function_top = 0;
    background_task->last_state = RTOS_state_idle;
}

RTOS_API(void, RTOS_vidStart, void)
{
#if (RTOS_COOKIE_ == 0)   // Timer0
    // enable compare-match INT
    Timer_vidEnableCmpINT(0);

    // turn on timer
    Timer_vidSetClock(Timer0_prescaler_div_64, 0);
#elif (RTOS_COOKIE_ == 2) // Timer2
    // enable compare-match INT
    Timer_vidEnableCmpINT(2);

    // turn on timer
    Timer_vidSetClock(Timer2_prescaler_div_64, 2);
#elif (RTOS_COOKIE_ == 1) // Timer1
    // enable compare-match INT
    Timer1_vidEnableCmpINT(Timer1_cmp_OCR1A);

    // turn on timer
    Timer1_vidSetClock(Timer1_prescaler_div_8);
#endif

    // enable global INT flag
    RTOS_ENABLE_GINT();
}

#if (RTOS_USE_EXTERNAL_STACK == 0)
RTOS_API(void, RTOS_vidRegisterTask, const RTOS_PC_t funcPtrFunctionCpy, const u16 u16Skip_msCpy, const u8 u8isPaused, const u8 u8IndexCpy)
#else // if using external stack
RTOS_API(void, RTOS_vidRegisterTask, const RTOS_PC_t funcPtrFunctionCpy, const u16 u16Skip_msCpy, const u8 u8isPaused, const RTOS_SP_t stackPtrStkCpy, const u16 u16StkLenCpy, const u8 u8IndexCpy)
#endif // RTOS_USE_EXTERNAL_STACK
{
    u8 sreg_I_bit = ( *(volatile u8*)0x5F ) & 0x80; // mask-out all bits except most left one

    RTOS_DISABLE_GINT();

    // sanity check
    // registering at the currently-running task is NOT allowed, because this will result in bad behavior
    if ( (u8IndexCpy >= RTOS_USER_TASKS_LEN) || (u8IndexCpy == (u8)(current_task - tasks)) )
    {
        if (sreg_I_bit)
        {
            RTOS_ENABLE_GINT();
        }

        return;
    }

    register RTOS_task_t* t = tasks + u8IndexCpy; // point at required task

#if (RTOS_USE_EXTERNAL_STACK == 0)
    const register RTOS_SP_t stk = stack[u8IndexCpy]; // point at required stack
    const register u16 stk_len = RTOS_STACK_LEN;

    t->stack_user_length = RTOS_USER_STACK_SIZE;
#else // if using external stack
    const register RTOS_SP_t stk = stackPtrStkCpy; // point at required stack
    const register u16 stk_len = u16StkLenCpy + RTOS_GP_REG_LEN + 2*sizeof(RTOS_PC_t);

    t->stack_user_length = u16StkLenCpy;
#endif // RTOS_USE_EXTERNAL_STACK

    // clear stack (implicitly GP registers too)
    for (register RTOS_SP_t s = stk; s < (stk + stk_len - 2*sizeof(RTOS_PC_t)); s += 1)
    {
        *s = 0;
    }

    // Reverse endian-ness.
    // The problem is that the value passed inside 'funcPtrFunctionCpy' has wrong endian-ness,
    // and when it's saved on the stack, it's reversed to the correct form,
    // but when it's reversed again during the context switching it reverts to the wrong form!.

    // save function address
    t->function_top = (RTOS_PC_t)( ((u16)funcPtrFunctionCpy >> 8) | ((u16)funcPtrFunctionCpy << 8) );

    // store the return address (address of first instruction)
    *(RTOS_PC_t*)(stk + stk_len - 2*sizeof(RTOS_PC_t)) = t->function_top;

    // store address of 'RTOS_vidResetCurrentTask' in last 2 bytes to allow normal 'return' by compiler
    // HACK: the one-liner solution below triggered a compiler bug on older version of GCC
    //*(RTOS_PC_t*)(stk + stk_len - sizeof(RTOS_PC_t)) = (RTOS_PC_t)( ((u16)RTOS_vidResetCurrentTask >> 8) | ((u16)RTOS_vidResetCurrentTask << 8) );
    stk[stk_len - 2] = (u8)( (u16)RTOS_vidResetCurrentTask >> 8 );
    stk[stk_len - 1] = (u8)( (u16)RTOS_vidResetCurrentTask );

    // point before PC and GP regs, so when it's pop-ed, stack pointer points at the beginning
    // of the stack (pop-ing increases pointer)
    t->stack_ptr = (RTOS_SP_t)(stk + stk_len - 2*sizeof(RTOS_PC_t) - RTOS_GP_REG_LEN - 1);

    // point at top of stack
    t->stack_top = stk;

    // initially no semaphores associated
    t->sem_addr = 0;

    t->ticks = u16Skip_msCpy;

    if (u16Skip_msCpy) // if initially delayed
    {
        t->last_state = RTOS_state_delayed;

        if (u8isPaused) // if also initially paused
        {
            t->state = RTOS_state_suspended;
        }
        else
        {
            t->state = RTOS_state_delayed;
        }
    }
    else               // if initially NOT delayed
    {
        t->last_state = RTOS_state_idle;

        if (u8isPaused) // if also initially paused
        {
            t->state = RTOS_state_suspended;
        }
        else
        {
            t->state = RTOS_state_idle;
        }
    }

    if (sreg_I_bit)
    {
        RTOS_ENABLE_GINT();
    }
}

RTOS_API_NAKED(void, RTOS_vidDeregisterTask, const u8 u8IndexCpy)
{
    RTOS_DISABLE_GINT();

    RTOS_SAVE_CURRENT_CONTEXT();

    // --- a context switch will happen if deleted task was the current one
    RTOS_vidHandleDelete(tasks + u8IndexCpy);
    // ------ //

    RTOS_RESTORE_NEW_CONTEXT();

    RTOS_ENABLE_GINT();

    RTOS_RET;
}

// skip the next 'N' slices, then timeout and start at slice 'N+1'
RTOS_API_NAKED(void, RTOS_vidSkip, register u16 u16Skip_msCpy)
{
    RTOS_DISABLE_GINT();

    RTOS_SAVE_CURRENT_CONTEXT();

    // --- a context switch will happen here
    RTOS_vidHandleDelay(u16Skip_msCpy);
    // ------ //

    RTOS_RESTORE_NEW_CONTEXT();

    RTOS_ENABLE_GINT();

    RTOS_RET;
}

RTOS_API_NAKED(void, RTOS_vidAcquireBinSem, register RTOS_sem_handle_t semHandleCpy)
{
    RTOS_DISABLE_GINT();

    RTOS_SAVE_CURRENT_CONTEXT();

    // --- task will be blocked and context switch will happen if semaphore is already acquired
    RTOS_vidHandleAcqBinSem(semHandleCpy);
    // ------ //

    RTOS_RESTORE_NEW_CONTEXT();

    RTOS_ENABLE_GINT();

    RTOS_RET;
}

RTOS_API_NAKED(void, RTOS_vidReleaseBinSem, register RTOS_sem_handle_t semHandleCpy)
{
    RTOS_DISABLE_GINT();

    RTOS_SAVE_CURRENT_CONTEXT();

    // --- task will be put in error state and context switch will happen if semaphore is already free
    RTOS_vidHandleRelBinSem(semHandleCpy);
    // ------ //

    RTOS_RESTORE_NEW_CONTEXT();

    RTOS_ENABLE_GINT();

    RTOS_RET;
}

RTOS_API_NAKED(void, RTOS_vidAcquireCountingSem, register RTOS_sem_handle_t semHandleCpy)
{
    RTOS_DISABLE_GINT();

    RTOS_SAVE_CURRENT_CONTEXT();

    // --- task will be blocked and context switch will happen if semaphore is completely acquired
    RTOS_vidHandleAcqCountingSem(semHandleCpy);
    // ------ //

    RTOS_RESTORE_NEW_CONTEXT();

    RTOS_ENABLE_GINT();

    RTOS_RET;
}

RTOS_API(void, RTOS_vidReleaseCountingSem, register RTOS_sem_handle_t semHandleCpy)
{
    RTOS_DISABLE_GINT();

    *semHandleCpy += 1; // increase the semaphore's count (release a count)

    RTOS_ENABLE_GINT();
}

RTOS_API_NAKED(void, RTOS_vidEnterCriticalSection, void)
{
    RTOS_DISABLE_GINT();

    RTOS_SAVE_CURRENT_CONTEXT();

    // --- task will be put in error state and context switch will happen if entering multiple critical sections
    RTOS_vidHandleEnterCriticalSection();
    // ------ //

    RTOS_RESTORE_NEW_CONTEXT();

    RTOS_ENABLE_GINT();

    RTOS_RET;
}

RTOS_API_NAKED(void, RTOS_vidLeaveCriticalSection, void)
{
    RTOS_DISABLE_GINT();

    RTOS_SAVE_CURRENT_CONTEXT();

    // --- task will be put in error state and context switch will happen if leaving unentered critical section
    RTOS_vidHandleLeaveCriticalSection();
    // ------ //

    RTOS_RESTORE_NEW_CONTEXT();

    RTOS_ENABLE_GINT();

    RTOS_RET;
}

RTOS_API_NAKED(void, RTOS_vidPauseCurrentTask, void)
{
    RTOS_DISABLE_GINT();

    RTOS_SAVE_CURRENT_CONTEXT();

    // --- a context switch will happen
    RTOS_vidHandlePause(current_task);
    // ------ //

    RTOS_RESTORE_NEW_CONTEXT();

    RTOS_ENABLE_GINT();

    RTOS_RET;
}

RTOS_API_NAKED(void, RTOS_vidPauseTask, const u8 u8IndexCpy)
{
    RTOS_DISABLE_GINT();

    RTOS_SAVE_CURRENT_CONTEXT();

    // --- a context switch will happen if required task is current one
    RTOS_vidHandlePause(tasks + u8IndexCpy);
    // ------ //

    RTOS_RESTORE_NEW_CONTEXT();

    RTOS_ENABLE_GINT();

    RTOS_RET;
}

RTOS_API(void, RTOS_vidResumeTask, const u8 u8IndexCpy)
{
    RTOS_DISABLE_GINT();

    // resuming background task is NOT allowed
    if (u8IndexCpy == (u8)(background_task - tasks))
    {
        RTOS_ENABLE_GINT();
        return;
    }

    // sanity check,
    // also resuming current task is NOT allowed because it's useless!
    if ( (u8IndexCpy >= RTOS_USER_TASKS_LEN) || (u8IndexCpy == (u8)(current_task - tasks)) )
    {
        RTOS_ENABLE_GINT();
        return;
    }

    // NOTE: resuming an already resumed task shouldn't be allowed
    //       because the second resume may put incorrect data in 'state'
    // ex: at start     -> last_state = idle, state = idle
    //     first cycle  -> state = resumed
    //     first resume -> state = last_state = idle (wrong)

    register RTOS_task_t* taskPtr = tasks + u8IndexCpy;

    if (taskPtr->state != RTOS_state_suspended)
    {
        RTOS_ENABLE_GINT();
        return;
    }

    // get previous/original task state
    // TODO: is this true ? this is a "blind" change of state, maybe more logic should be added
    taskPtr->state = taskPtr->last_state;

    RTOS_ENABLE_GINT();
}

// this is also used when normal 'return' is used, will switch context after resetting task attributes
RTOS_API_NAKED(void, RTOS_vidResetCurrentTask, void)
{
    RTOS_DISABLE_GINT();

    RTOS_POINT_SP_TO_PLAYFIELD();

    // --- a context switch will happen after resetting current task's attributes
    RTOS_vidHandleReset(current_task);
    // ------ //

    RTOS_RESTORE_NEW_CONTEXT();

    RTOS_ENABLE_GINT();

    RTOS_RET;
}

RTOS_API_NAKED(void, RTOS_vidResetTask, const u8 u8IndexCpy)
{
    RTOS_DISABLE_GINT();

    RTOS_SAVE_CURRENT_CONTEXT(); // needed because we may pause other tasks than current one

    // --- a context switch will happen after resetting task's attributes
    RTOS_vidHandleReset(tasks + u8IndexCpy);
    // ------ //

    RTOS_RESTORE_NEW_CONTEXT();

    RTOS_ENABLE_GINT();

    RTOS_RET;
}

RTOS_API_NAKED(void, RTOS_vidGiveUp, void)
{
    RTOS_DISABLE_GINT();

    RTOS_SAVE_CURRENT_CONTEXT();

#if (RTOS_OVERFLOW_CHECK != 0)
    RTOS_vidHandleOverflow();
#endif // RTOS_OVERFLOW_CHECK

    RTOS_vidGetNextTask();

    RTOS_RESTORE_NEW_CONTEXT();

    RTOS_ENABLE_GINT();

    RTOS_RET;
}

RTOS_API(u16, RTOS_u16GetTicksCount, void)
{
    return RTOS_u16Ticks_ms;
}

RTOS_API(RTOS_task_state_t, RTOS_enumGetCurrentTaskState, void)
{
    return current_task->state;
}

RTOS_API(RTOS_task_state_t, RTOS_enumGetTaskState, const u8 u8IndexCpy)
{
    // sanity check
    if (u8IndexCpy >= RTOS_USER_TASKS_LEN)
    {
        return RTOS_state_uninitialized;
    }

    return tasks[u8IndexCpy].state;
}

#if (RTOS_USE_ERROR_CALLBACK != 0)
RTOS_API(void, RTOS_vidRegisterErrorCB, const RTOS_error_CB_t funcPtrErrorCBCpy)
{
    RTOS_CB_error = funcPtrErrorCBCpy;
}

RTOS_API(void, RTOS_vidDeregisterErrorCB, void)
{
    RTOS_CB_error = 0;
}

RTOS_API(void, RTOS_vidRegisterCriticalErrorCB, const RTOS_error_CB_t funcPtrErrorCBCpy)
{
    RTOS_CB_critical_error = funcPtrErrorCBCpy;
}

RTOS_API(void, RTOS_vidDeregisterCriticalErrorCB, void)
{
    RTOS_CB_critical_error = 0;
}
#endif // RTOS_USE_ERROR_CALLBACK


#endif // RTOS_COOKIE_

