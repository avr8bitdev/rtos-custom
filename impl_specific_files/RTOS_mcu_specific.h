/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


// These are private microcontroller-based functions/macros to do low-level operations in asm.


#ifndef RTOS_MCU_SPECIFIC_H_
#define RTOS_MCU_SPECIFIC_H_


// macros to push 1 GP register
#define RTOS_PUSH_REG(n) asm volatile("push R" #n " \t\n")

// macros to push status register
#define RTOS_PUSH_SREG()              \
    asm volatile("in R0, 0x3F \t\n"); \
    RTOS_PUSH_REG(0)

// macros to pop 1 GP register
#define RTOS_POP_REG(n) asm volatile("pop R" #n " \t\n")

// macros to pop status register
#define RTOS_POP_SREG()          \
    RTOS_POP_REG(0); \
    asm volatile("out 0x3F, R0 \t\n")

// macros to disable/enable global interrupt
#define RTOS_DISABLE_GINT() asm volatile("cli")
#define RTOS_ENABLE_GINT()  asm volatile("sei")

// macros to return from ISR, or a function if it's naked
#define RTOS_RETI asm volatile("reti")
#define RTOS_RET  asm volatile("ret")

// macro to push whole task context (GP regs + SREG)
/*
 NOTE: the AVR calling convention used by GCC assumes 'R1' will be = 0
       at the beginning of the ISR
*/
#define RTOS_PUSH_MACHINE_STATE() \
     RTOS_PUSH_REG(0);            \
     /* push SREG */              \
     RTOS_PUSH_SREG();            \
     RTOS_PUSH_REG(1);            \
     asm volatile("clr R1 \t\n"); \
     RTOS_PUSH_REG(2);            \
     RTOS_PUSH_REG(3);            \
     RTOS_PUSH_REG(4);            \
     RTOS_PUSH_REG(5);            \
     RTOS_PUSH_REG(6);            \
     RTOS_PUSH_REG(7);            \
     RTOS_PUSH_REG(8);            \
     RTOS_PUSH_REG(9);            \
     RTOS_PUSH_REG(10);           \
     RTOS_PUSH_REG(11);           \
     RTOS_PUSH_REG(12);           \
     RTOS_PUSH_REG(13);           \
     RTOS_PUSH_REG(14);           \
     RTOS_PUSH_REG(15);           \
     RTOS_PUSH_REG(16);           \
     RTOS_PUSH_REG(17);           \
     RTOS_PUSH_REG(18);           \
     RTOS_PUSH_REG(19);           \
     RTOS_PUSH_REG(20);           \
     RTOS_PUSH_REG(21);           \
     RTOS_PUSH_REG(22);           \
     RTOS_PUSH_REG(23);           \
     RTOS_PUSH_REG(24);           \
     RTOS_PUSH_REG(25);           \
     RTOS_PUSH_REG(26);           \
     RTOS_PUSH_REG(27);           \
     RTOS_PUSH_REG(28);           \
     RTOS_PUSH_REG(29);           \
     RTOS_PUSH_REG(30);           \
     RTOS_PUSH_REG(31)

// macro to save current SP in 'current_task->stack_ptr'
#define RTOS_SAVE_SP()                                                                      \
    asm volatile(                                                                           \
                 /* here 'current_task' is replaced by its address, i.e '&current_task' */  \
                 "lds R30, current_task     \t\n" /* mov R30, byte [&current_task] */       \
                 "lds R31, (current_task+1) \t\n" /* mov R31, byte [(&current_task) + 1] */ \
                 /* store lower byte of SP in lower address */                              \
                 "in R29, 0x3D \t\n" /* SP low */                                           \
                 "st Z, R29 \t\n"                                                           \
                 /* store higher byte of SP in higher address */                            \
                 "in R29, 0x3E \t\n" /* SP high */                                          \
                 "std Z+1, R29 \t\n"                                                        \
                 /* -------------------- */                                                 \
                 /* | SP low | SP high | */                                                 \
                 /* -------------------- */                                                 \
                 /*    (Z)     (Z+1)     */                                                 \
                )

// macro to point SP at the Playfield
#define RTOS_POINT_SP_TO_PLAYFIELD()                                                  \
    asm volatile(                                                                     \
                 /* here 'RTOS_playfield' is the address of the first member */       \
                 /* mov R31, low_byte(RTOS_playfield + RTOS_PLAYFIELD_LENGTH - 1) */  \
                 "ldi R31, lo8(" STR(RTOS_playfield+RTOS_PLAYFIELD_LENGTH-1) ") \t\n" \
                 /* store lower address nibble in lower byte of SP */                 \
                 "out 0x3D, R31 \t\n" /* SP low */                                    \
                 /* mov R31, high_byte(RTOS_playfield + RTOS_PLAYFIELD_LENGTH - 1) */ \
                 "ldi R31, hi8(" STR(RTOS_playfield+RTOS_PLAYFIELD_LENGTH-1) ") \t\n" \
                 /* store higher address nibble in higher byte of SP */               \
                 "out 0x3E, R31 \t\n" /* SP high */                                   \
                 /* -------------------------------------- */                         \
                 /* | high addr nibble | low addr nibble | */                         \
                 /* -------------------------------------- */                         \
                 /*       SP high            SP low        */                         \
                )

// macro to save: {PC (implicit by H/W), GP regs, SREG, SP}, then, point at Playfield
#define RTOS_SAVE_CURRENT_CONTEXT()                                        \
    /* push machine state (GP regs + SREG), PC is already pushed by H/W */ \
    RTOS_PUSH_MACHINE_STATE();                                             \
    /* save stack pointer of current task */                               \
    RTOS_SAVE_SP();                                                        \
    /* point SP to the Playfield so we can move around safely */           \
    RTOS_POINT_SP_TO_PLAYFIELD()

// macro to increment ticks counter
#define RTOS_INC_TICKS_COUNTER()                                                                    \
    asm volatile(                                                                                   \
                 /* here 'RTOS_u16Ticks_ms' is replaced by its address, i.e '&RTOS_u16Ticks_ms' */  \
                 "lds R30, RTOS_u16Ticks_ms \t\n"     /* mov R30, byte [&RTOS_u16Ticks_ms] */       \
                 "lds R31, (RTOS_u16Ticks_ms+1) \t\n" /* mov R31, byte [(&RTOS_u16Ticks_ms) + 1] */ \
                 "adiw R30, 1 \t\n"                   /* R31:R30 += 1 */                            \
                 "sts RTOS_u16Ticks_ms, R30 \t\n"     /* mov byte [&RTOS_u16Ticks_ms], R30 */       \
                 "sts (RTOS_u16Ticks_ms+1), R31 \t\n" /* mov byte [(&RTOS_u16Ticks_ms) + 1], R31 */ \
                )

// macro to pop whole task context
#define RTOS_POP_MACHINE_STATE() \
        RTOS_POP_REG(31);        \
        RTOS_POP_REG(30);        \
        RTOS_POP_REG(29);        \
        RTOS_POP_REG(28);        \
        RTOS_POP_REG(27);        \
        RTOS_POP_REG(26);        \
        RTOS_POP_REG(25);        \
        RTOS_POP_REG(24);        \
        RTOS_POP_REG(23);        \
        RTOS_POP_REG(22);        \
        RTOS_POP_REG(21);        \
        RTOS_POP_REG(20);        \
        RTOS_POP_REG(19);        \
        RTOS_POP_REG(18);        \
        RTOS_POP_REG(17);        \
        RTOS_POP_REG(16);        \
        RTOS_POP_REG(15);        \
        RTOS_POP_REG(14);        \
        RTOS_POP_REG(13);        \
        RTOS_POP_REG(12);        \
        RTOS_POP_REG(11);        \
        RTOS_POP_REG(10);        \
        RTOS_POP_REG(9);         \
        RTOS_POP_REG(8);         \
        RTOS_POP_REG(7);         \
        RTOS_POP_REG(6);         \
        RTOS_POP_REG(5);         \
        RTOS_POP_REG(4);         \
        RTOS_POP_REG(3);         \
        RTOS_POP_REG(2);         \
        RTOS_POP_REG(1);         \
        /* pop SREG */           \
        RTOS_POP_SREG();         \
        RTOS_POP_REG(0)

// macro to restore current SP from 'current_task'->'stack_ptr'
#define RTOS_RESTORE_SP()                                                                  \
    asm volatile(                                                                          \
                 /* here 'current_task' is replaced by its address, i.e '&current_task' */ \
                 "lds R30, current_task     \t\n" /* mov R30, [&current_task] */           \
                 "lds R31, (current_task+1) \t\n" /* mov R31, [(&current_task) + 1] */     \
                 /* store lower address nibble in lower byte of SP */                      \
                 "ld R29, Z \t\n"                                                          \
                 "out 0x3D, R29 \t\n" /* SP low */                                         \
                 /* store higher address nibble in higher byte of SP */                    \
                 "ldd R29, Z+1 \t\n"                                                       \
                 "out 0x3E, R29 \t\n" /* SP high */                                        \
                 /* -------------------------------------- */                              \
                 /* | high addr nibble | low addr nibble | */                              \
                 /* -------------------------------------- */                              \
                 /*       SP high            SP low        */                              \
                )

// macro to restore PC, GP regs, SREG, SP
#define RTOS_RESTORE_NEW_CONTEXT()       \
    /* get stack pointer of next task */ \
    RTOS_RESTORE_SP();                   \
    /* restore registers */              \
    RTOS_POP_MACHINE_STATE()
// ------------------------------------------------ //


#endif /* RTOS_MCU_SPECIFIC_H_ */

