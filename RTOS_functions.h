/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef RTOS_FUNCTIONS_H_
#define RTOS_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"
#include "RTOS_public_types.h"
#include "RTOS_config.h"

// --- convenience defines --- //
#define RTOS_NO_DELAY 0
#define RTOS_HIGHEST_PRIORITY 0

#define RTOS_PAUSED 1
#define RTOS_RESUMED 0

#define RTOS_SEM_ACQUIRED 0
#define RTOS_SEM_FREE     1
// --------------------------- //

// --- task declaration and definition --- //
#define RTOS_DECL_TASK(func) void func(void) __attribute__((used, noinline))
#define RTOS_TASK(func) void func(void)
// -------------------------------------- //

// --- external stack declaration --- //
#if (RTOS_USE_EXTERNAL_STACK != 0)
    #define RTOS_CREATE_STACK(identifier, user_len) u8 identifier[user_len + 33 + 2*sizeof(RTOS_PC_t)] __attribute__((used, externally_visible))
#endif // RTOS_USE_EXTERNAL_STACK
// ---------------------------------- //

void RTOS_vidInit(void);
void RTOS_vidStart(void);

#if (RTOS_USE_EXTERNAL_STACK == 0)
void RTOS_vidRegisterTask(const RTOS_PC_t funcPtrFunctionCpy, const u16 u16Skip_msCpy, const u8 u8isPaused, const u8 u8IndexCpy);
#else
void RTOS_vidRegisterTask(const RTOS_PC_t funcPtrFunctionCpy, const u16 u16Skip_msCpy, const u8 u8isPaused, const RTOS_SP_t stackPtrStkCpy, const u16 u16StkLenCpy, const u8 u8IndexCpy);
#endif // RTOS_USE_EXTERNAL_STACK

void RTOS_vidDeregisterTask(const u8 u8IndexCpy);

// skip the next 'N' slices, then timeout and start at slice 'N+1'
void RTOS_vidSkip(register u16 u16Skip_msCpy);
#define RTOS_vidSleep(n) RTOS_vidSkip(n - 1)

void RTOS_vidAcquireBinSem(register RTOS_sem_handle_t semHandleCpy);
#define RTOS_vidLockBinSem(sem) RTOS_vidAcquireBinSem(&sem)
void RTOS_vidReleaseBinSem(register RTOS_sem_handle_t semHandleCpy);
#define RTOS_vidFreeBinSem(sem) RTOS_vidReleaseBinSem(&sem)

void RTOS_vidAcquireCountingSem(register RTOS_sem_handle_t semHandleCpy);
#define RTOS_vidGetFromCountingSem(sem) RTOS_vidAcquireCountingSem(&sem)
void RTOS_vidReleaseCountingSem(register RTOS_sem_handle_t semHandleCpy);
#define RTOS_vidGiveToCountingSem(sem) RTOS_vidReleaseCountingSem(&sem)

void RTOS_vidEnterCriticalSection(void);
void RTOS_vidLeaveCriticalSection(void);

void RTOS_vidPauseCurrentTask(void);
#define RTOS_vidPause() RTOS_vidPauseCurrentTask()
void RTOS_vidPauseTask(const u8 u8IndexCpy);
void RTOS_vidResumeTask(const u8 u8IndexCpy);

void RTOS_vidResetCurrentTask(void);
#define RTOS_vidReturn() RTOS_vidResetCurrentTask()
void RTOS_vidResetTask(const u8 u8IndexCpy);

void RTOS_vidGiveUp(void);

u16 RTOS_u16GetTicksCount(void);

RTOS_task_state_t RTOS_enumGetCurrentTaskState(void);
RTOS_task_state_t RTOS_enumGetTaskState(const u8 u8IndexCpy);

#if (RTOS_USE_ERROR_CALLBACK != 0)
void RTOS_vidRegisterErrorCB(const RTOS_error_CB_t funcPtrErrorCBCpy);
void RTOS_vidDeregisterErrorCB(void);
void RTOS_vidRegisterCriticalErrorCB(const RTOS_error_CB_t funcPtrErrorCBCpy);
void RTOS_vidDeregisterCriticalErrorCB(void);
#endif // RTOS_USE_ERROR_CALLBACK


#endif /* RTOS_FUNCTIONS_H_ */

